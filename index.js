const express = require('express');
const path = require('path');
const axios = require('axios');

const app = express();

app.use(express.static(path.join(__dirname, 'dist')));
app.use('/assets', express.static(path.join(__dirname, 'assets')))

app.get('/api/search/:query', (req, res) => {
    res.setHeader('content-type', 'application/json');

    axios.get(`https://api.mercadolibre.com/sites/MLA/search?q=${req.params.query}`)
        .then(({data: {results}}) => {
            res.json(results);
        })
        .catch(error => {
            res.sendStatus(400);
            res.json({error: 'error'})
        });
});

app.get('/api/product/:id', (req, res) => {
    Promise.all([
        axios.get('https://api.mercadolibre.com/items/' + req.params.id),
        axios.get('https://api.mercadolibre.com/items/' + req.params.id + '/description')
    ]).then(([{data: general}, {data: description}]) => {
        res.json({general, description});
    });
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist', 'index.html'));
});

app.listen(3000);