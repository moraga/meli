import React from 'react';
import axios from 'axios';
import { NavBar } from './nav';

export class ProductPage extends React.Component {
    constructor(props) {
        super(props);
        const {id} = this.props.match.params;
        this.state = {id};
    }

    componentWillMount() {
        axios.get(`/api/product/${this.state.id}`)
            .then(({data}) => {
                this.setState(data);
            });
    }

    render() {
        return (
            <div>
                <NavBar />
                <div className="container">
                    <div className="bg-white p-3">
                        {this.state.general ? <ProductDetails {...this.state.general} /> : ''}
                        {this.state.description ? <ProductDescription {...this.state.description} /> : ''}
                    </div>
                </div>
            </div>
        )
    }
}

class ProductDetails extends React.Component {
    render() {
        return (
            <div className="d-flex">
                <div className="product-imgs">
                    {this.props.pictures.map(item => (
                        <img src={item.secure_url} />
                    ))}
                </div>
                <div>
                    <h1>{this.props.title}</h1>
                    <h4 className="product-price-lg">{this.props.price}</h4>
                    <button className="btn btn-primary btn-block">Comprar</button>
                </div>
            </div>
        )
    }
}

class ProductDescription extends React.Component {
    render() {
        return (
            <div className="py-4">
                <div className="h3">Descripción del producto</div>
                {this.props.plain_text}
            </div>
        )
    }
}