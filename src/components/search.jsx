import React from 'react';
import { Link } from 'react-router-dom';
import { NavBar } from './nav';
import axios from 'axios';

export class SearchPage extends React.Component {
    constructor(props) {
        super(props);

        let query = this.props.location.search.substr(3);

        this.state = {
            query,
            results: []
        };
    }

    componentDidMount() {
        axios.get(`/api/search/${this.state.query}`)
            .then(({data: results}) => {
                this.setState({results});
            })
    }

    render() {
        return (
            <div>
                <NavBar />
                <div className="container">
                    <div className="bg-white">
                        {this.state.results.map((result, key) => <Result key={key} {...result} />)}
                    </div>
                </div>
            </div>
        )
    }
}

class Result extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const link = "/items/" + this.props.id;

        return (
            <Link className="media d-flex" to={link}>
                <img src={this.props.thumbnail} />
                <div className="flex-grow-1">
                    <h4 className="product-price">{this.props.price}</h4>
                    <h3 className="product-title">{this.props.title}</h3>
                </div>
                <div>
                    {this.props.address.city_name}
                </div>
            </Link>
        )
    }
}