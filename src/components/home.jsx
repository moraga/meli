import React from 'react';
import { NavBar } from './nav';
import { Search } from './search';

export class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            query: null
        };

        this.processQuery = this.processQuery.bind(this);
    }

    processQuery(query) {
        this.setState({query});
    }

    render() {
        return (
            <div>
                <NavBar onSubmit={this.processQuery} />
                {this.state.query != null ? <Search key={this.state.query} query={this.state.query}/> : null}
            </div>
        )
    }
}