import React from 'react';
import { Redirect } from 'react-router-dom';

export class NavBar extends React.Component {
    render() {
        return (
            <div>
                <nav className="navbar">
                    <div className="container d-flex">
                        <div className="navbar-brand"></div>
                        <SearchBar />
                    </div>
                </nav>
                <div className="container">
                    <Breadcrumb />
                </div>
            </div>
        )
    }
}

export class SearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            value: ''
        };
        
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({redirect: this.state.value});
    }

    handleChange({target: {value}}) {
        this.setState({value});
    }

    render() {
        const { redirect } = this.state;

        if (redirect) {
            const path = "/items?q=" + redirect;

            return <Redirect to={path} />
        }

        return (
            <form className="navbar-form flex-grow-1 d-flex" onSubmit={this.handleSubmit}>
                <input className="form-control" value={this.state.value} onChange={this.handleChange} placeholder="Nunca dejes de buscar"/>
                <button className="btn btn-light">
                    <span className="icon icon-search"></span>
                </button>
            </form>
        )
    }
}

export class Breadcrumb extends React.Component {
    render() {
        return (
            <nav>
                <ol className="breadcrumb">
                    <li>Eletrônica, Áudio y Video</li>
                    <li>iPod</li>
                    <li>Reproductores</li>
                    <li>32 GB</li>
                </ol>
            </nav>
        )
    }
}