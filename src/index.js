import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { HomePage } from './components/home';
import { SearchPage } from './components/search';
import { ProductPage } from './components/product';
import ReactDOM from 'react-dom';
import './style.scss';


class App extends React.Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" component={HomePage} />
                <Route exact path="/items" component={SearchPage}/>
                <Route path="/items/:id" component={ProductPage} />
            </Switch>
        )
    }
}

ReactDOM.render((
    <BrowserRouter>
        <App />
    </BrowserRouter>
), document.getElementById('root'));